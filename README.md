# Creador de diagramas UML, por medio de XML. #

## US  ##
1- Como usuario, quiero poder visualizar diagramas de casos de uso.

- Visualizar actores y casos de uso.

2- Como usuario, quiero poder visualizar diagramas de clases

 - Visualizar clases con sus atributos.
 - Ver relaciones entre clases
 - Visualizar métodos

3- Como usuario, para poder visualizar un diagrama, quiero poder describirlo en formato XML.

- Crear un cuadro de texto que permita recibir un input en XML.

4- Como usuario, quiero que los diagramas generados sean compactos.

- Ocupar el menor espacio posible, optimizando los cruces de líneas.

5- Como usuario, quiero tener un botón que me permita visualizar el XML como diagrama.

- Al hacer click en el botón, se debe generar automáticamente el diagrama

6- Como usuario, no quiero esperar más de 10 segundos para generar el diagrama.

7- Como usuario, quiero poder elegir la carpeta de destino para guardar la imagen del diagrama.

- Abrir un diálogo de archivo cuando el usuario quiera guardar el diagrama.

8- Como usuario, quiero que el programa no se caiga.

- En caso de algún error, el programa avisa el tipo de error.

9- Como usuario, quiero poder visualizar diagramas que superen las dimensiones de la pantalla.

- En caso de generar un diagrama muy grande, se podrá navegar por él con scroll bars.

10- Como usuario, quiero que la interfaz sea intuitiva.

- Fácil de usar (interfaz minimalista y directa al grano).
- Agradable a la vista.

11- Como usuario, quiero que los diagramas de clase se vean ordenados.

- Ordenar las clases de manera jerárquica.
- Pocos cruces de líneas.

12- Como usuario, quiero que los diagramas de casos de uso se vean ordenados.

- Que haya el menor número posible de cruces de líneas.

13- Como usuario, quiero que el programa me alerte si el XML está mal escrito.

- Si el usuario intenta generar el diagrama a partir de un XML con error de sintaxis, aparecerá una ventana de error.

14- Como usuario, quiero poder agregar una nota cualquier elemento del diagrama.

- El usuario puede hacer click en un elemento y agregar una nota.


##ENTREGA 1:##

##Hecho:##

* Parsear XML para armar un diagrama de clases -> Gonzalo
* Parsear XML para armar un diagrama de casos de uso -> Vicente
* Hacer visible en consola el diagrama de clases -> Pablo
* Hacer visible en consola el diagrama de casos de uso -> Gonzalo
* Backend Diagramas -> Los 3
* Backend Diagrama de casos de uso -> Pablo
* Backend Diagrama de clases -> Los 3
* Crear una interfaz gráfica que permita recibir el XML -> Vicente

Diagrama UML de clases del programa:

![Class Diagram2.jpg](https://bitbucket.org/repo/7ropdq/images/3263372872-Class%20Diagram2.jpg)