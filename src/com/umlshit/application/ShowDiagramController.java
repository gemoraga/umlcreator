package com.umlshit.application;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.util.FileUtils;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ShowDiagramController implements Initializable {

	@FXML MenuItem addNote;
	@FXML ContextMenu contextMenuDB;
	@FXML public ScrollPane diagramPane;
	private Stage s;
	private String xml;
	private Diagram d;
	private boolean canAddNote;
	private List<TextArea> notes;

	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		notes = new ArrayList<TextArea>();
		diagramPane.addEventHandler(MouseEvent.MOUSE_CLICKED,
			    new EventHandler<MouseEvent>() {
			        @Override public void handle(MouseEvent e) {
			            if (e.getButton() == MouseButton.PRIMARY && canAddNote) {			            	
			            	newNote(e.getX(), e.getY(), "");
			                //mouseCoords = new Point2D.Double(e.getX(), e.getY());
			            }
			        }
			});
		final EventHandler<KeyEvent> keyPressed = new EventHandler<KeyEvent>() {
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.CONTROL) {
                    canAddNote=true;
                    keyEvent.consume();
                }
            }
        };
        
        EventHandler<KeyEvent> keyReleased = new EventHandler<KeyEvent>() {
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.CONTROL) {
                    canAddNote=false;
                    keyEvent.consume();
                }
            }
        };
        
        diagramPane.setOnKeyReleased(keyReleased);
        diagramPane.setOnKeyPressed(keyPressed);
		
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
            	diagramPane.requestFocus();
            }
       });
	}
	
	
	public void setStage(Stage s) {
		this.s = s;
	}
	
	public void setDiagram(Diagram d) {
		this.d = d;
	}
	
	public void setXml(String xml) {
		this.xml = xml;
	}
	
	@FXML
	private void exportPng() throws IOException {
		Scene scene = s.getScene();
		Node sp =  scene.lookup("#diagramPane");
		WritableImage image = sp.snapshot(new SnapshotParameters(), null);
		FileUtils.SavePNG(s, image, "PNG Files (*.png)", "*.png");
	}
	
	@FXML
	private void saveUml() throws IOException {

		StringBuilder sb = new StringBuilder();
		
		try {
			
			List<ConnectableObject> conns = d.getConnectables();
			sb.append(xml.replace("\n", "") + "\n");
			for (ConnectableObject c : conns) {
				sb.append(c.getId() + ":" + c.getX() + "," + c.getY() + ";");
			}
			
			if (notes.size() != 0) {
				sb.append("\n");
				for (TextArea ta : notes) {
					sb.append(ta.getText().replace("\n", "\\w") + ":"
							+ ta.getLayoutX() + "," + ta.getLayoutY() + ";");
				}
			}
			FileUtils.SaveFile(s, sb.toString(), "UML files (*.uml)", "*.uml");
		} catch (Exception e) {
		  // report
		}
	}
	@FXML public void cerrar(){
		s.close();
	}
	
	public void newNote(double x, double y, String text) {
		
		final TextArea ta = new TextArea();
		
		ta.getStylesheets().add("com/umlshit/application/note.css");
		ta.setEditable(true);
		
		ta.setMinWidth(100);
		ta.setMinHeight(70);
		ta.setPrefWidth(100);
		ta.setPrefHeight(70);
		
		ta.setText(text);
		ta.setWrapText(true);
		ta.setLayoutX(x);
		ta.setLayoutY(y);
		
		ta.setOpacity(0.7);
		
		
		Pane canvas = (Pane) diagramPane.getContent();
		canvas.getChildren().add(ta);
		notes.add(ta);
		ta.addEventHandler(MouseEvent.MOUSE_CLICKED,
			    new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						if (e.getButton() == MouseButton.PRIMARY && canAddNote) {

							((Pane) ta.getParent()).getChildren().remove(ta);
							canAddNote = false;
							notes.remove(ta);
						}
						// mouseCoords = new Point2D.Double(e.getX(), e.getY());

					}
		});
		
		ta.addEventHandler(MouseEvent.MOUSE_ENTERED,
			    new EventHandler<MouseEvent>() {
			        @Override public void handle(MouseEvent e) {
			            ta.setOpacity(1);
			        }
		});
		
		ta.addEventHandler(MouseEvent.MOUSE_EXITED,
			    new EventHandler<MouseEvent>() {
			        @Override public void handle(MouseEvent e) {
			            ta.setOpacity(0.7);
			        }
		});
		
		
	}
}
