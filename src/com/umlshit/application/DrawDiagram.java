package com.umlshit.application;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;

import com.umlshit.application.customControls.ActorControl;
import com.umlshit.application.customControls.ClassControl;
import com.umlshit.application.customControls.DiagramLine;
import com.umlshit.application.customControls.UseCaseControl;
import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.Connection.Type;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.Class.ClassBox;
import com.umlshit.backend.Class.ClassDiagram;
import com.umlshit.backend.UC.Actor;
import com.umlshit.backend.UC.UCBox;
import com.umlshit.backend.UC.UCDiagram;
import com.umlshit.backend.util.DiagramUtils;

public class DrawDiagram {
	
	@SuppressWarnings("deprecation")
	public static Diagram drawUCDiagram(ScrollPane diagramPane, Diagram diagram, boolean order) {
		Pane canvas = new Pane();
		diagramPane.setContent(canvas);
		Random rand = new Random();
		List<ConnectableObject> conns = diagram.getConnectables();
		List<VBox> controls = new ArrayList<VBox>();
		int bestScore = Integer.MAX_VALUE;
		UCDiagram bestDiagram = (UCDiagram) diagram.Clone();
		DropShadow ds1 = new DropShadow();
        ds1.setOffsetY(4.0f);
        ds1.setOffsetX(4.0f);
        ds1.setColor(Color.GRAY);
		for (ConnectableObject c : conns) {
			VBox control;
			if (c.getClass().equals(Actor.class)) {
				Actor actor = (Actor) c;
				ActorControl ac = new ActorControl(actor);
				ac.setTitle();
				//ac.setStyle("-fx-background-image: url('/Resources/png/a" + rand.nextInt(9) + ".png')");
				control = ac;
			} else {
				UCBox caja = (UCBox) c;
				UseCaseControl uc = new UseCaseControl(caja);
				uc.setTitle();

				uc.setEffect(ds1);
				control = uc;
			}


			
			canvas.getChildren().add(control);
			control.impl_processCSS(true);
			c.resize(control.prefWidth(-1), control.prefHeight(-1));
			controls.add(control);
		}

		if (order) {
		for (int i = 0; i < 200; i++) {
			int actorCountPrimary = 0;
			int actorCountSecondary = 0;
			int ucCount = 0;
			ArrayList<ConnectableObject> secActors = new ArrayList<ConnectableObject>();
			for (ConnectableObject c : conns) {
				if (c.getClass().equals(Actor.class)) {
					Actor a = (Actor) c;
					if (a.getType().equals("Primary")) {
						c.moveTo(30, (actorCountPrimary) * 200 + 50);
						actorCountPrimary++;
					} else if (a.getType().equals("Secondary")) {
						c.moveTo(700 + 30 + 150,
								(actorCountSecondary) * 200 + 50);
						secActors.add(c);
						actorCountSecondary++;
					}
				} else {
					c.moveTo(rand.nextInt(100) + 200, rand.nextInt(100) + 200);
					ucCount++;
				}
			}
			float height = Math.max((actorCountPrimary - 1),
					(actorCountSecondary - 1)) * 200 + 50 * 2;
			float width = ucCount * 40 + 400;
			
			for (ConnectableObject c : secActors) {
				c.moveTo(width + 30 + 150, c.getY());
			}

			

			diagram.resize(width, height);

			DiagramUtils.fruchtermanReingold(diagram,2,1000, -1, 500, 150, 2,  1, false,
					true);

			DiagramUtils.fruchtermanReingold(diagram,2,10, -1, 500, 150, 1.3,
					(conns.size()+1) * 10, false, false);

			setUCBackendLines(diagram);

			// CHECK SCORE
			int currentScore = DiagramUtils.score(diagram, 1, 5);
			if (currentScore < bestScore) {

				bestScore = currentScore;
				bestDiagram = (UCDiagram) diagram.Clone();
			}
		}
		}
		
		if (!order) {
			bestDiagram = (UCDiagram) diagram;
			setUCBackendLines(bestDiagram);
		}
		
		for (int j = 0; j < controls.size(); j++) {
			controls.get(j).relocate(					
					bestDiagram.getConnectables().get(j).getX(),
					bestDiagram.getConnectables().get(j).getY());
		}

		drawUCLines(bestDiagram, canvas);
		return bestDiagram;
	}

	private static void setUCBackendLines(Diagram diagram) {
		List<Connection> connections = diagram.getConnections();
		for (Connection c : connections) {
			String from = c.getFrom();
			String to = c.getTo();
			ConnectableObject obj1 = diagram.getConnById(from);
			ConnectableObject obj2 = diagram.getConnById(to);
			Point2D start = new Point2D.Double(), goal = new Point2D.Double();
			if (obj1.getClass().equals(Actor.class)) {
				Actor a = (Actor) obj1;
				if (a.getType().equals("Primary")) {
					start = new Point2D.Double(a.getX() + a.getWidth(),
							a.getY() + a.getHeight() / 2);
				} else if (a.getType().equals("Secondary")) {
					start = new Point2D.Double(a.getX(), a.getY()
							+ a.getHeight() / 2);
				}
			} else {
				double posX = obj1.getX() < obj2.getX() ? obj1.getX()
						+ obj1.getWidth() : obj1.getX();
				double posY = obj1.getY() + obj1.getHeight() / 2;
				start = new Point2D.Double(posX, posY);
			}

			if (obj2.getClass().equals(Actor.class)) {
				Actor a = (Actor) obj2;
				if (a.getType().equals("Primary")) {
					goal = new Point2D.Double(a.getX() + a.getWidth(), a.getY()
							+ a.getHeight() / 2);
				} else if (a.getType().equals("Secondary")) {
					goal = new Point2D.Double(a.getX(), a.getY()
							+ a.getHeight() / 2);
				}
			} else {
				double posX = obj2.getX() < obj1.getX() ? obj2.getX()
						+ obj2.getWidth() : obj2.getX();
				double posY = obj2.getY() + obj2.getHeight() / 2;
				goal = new Point2D.Double(posX, posY);
			}
			c.setStart(start);
			c.setGoal(goal);
			/*
			 * float posX1 = (float) (obj1.getX()); float posY1 = (float)
			 * (obj1.getY()); float posX2 = (float) (obj2.getX()); float posY2 =
			 * (float) (obj2.getY());
			 */
		}
	}

	private static void drawUCLines(Diagram diagram, Pane canvas) {
		List<Connection> connections = diagram.getConnections();
		for (int i = 0; i < connections.size(); i++) {
			Connection c = connections.get(i);
			ConnectableObject from = diagram.getConnById(c.getFrom());
			ConnectableObject to = diagram.getConnById(c.getTo());
			double X1 = from.getX() + from.getWidth()/2;
			double Y1 = from.getY() + from.getHeight()/2;
			double X2 = to.getX() + to.getWidth()/2;
			double Y2 = to.getY() + to.getHeight()/2;
					
			//double dx = X2 - X1, dy = Y2 - Y1;
			//double angle = Math.atan2(dy, dx) * 180.0 / Math.PI - 90;
			Line2D l1 = new Line2D.Double(X1,Y1,X2,Y2);
			for (Line2D l : to.getBounds()) {
				Point2D intersection = getLineIntersection(l1, l);
				if (intersection != null){
					X2 = intersection.getX();
					Y2 = intersection.getY();
					break;
				}
			}
			for (Line2D l : from.getBounds()) {
				Point2D intersection = getLineIntersection(l1, l);
				if (intersection != null){
					X1 = intersection.getX();
					Y1 = intersection.getY();
					break;
				}
			}
			Type type = c.getType();
			Point2D start = new Point2D.Double(X1, Y1);
			Point2D goal = new Point2D.Double(X2, Y2);
			
			DiagramLine line = DiagramLine.ucLine(start, goal, type);
			canvas.getChildren().add(line);
		}
	}
	
	public static Point2D.Double getLineIntersection(Line2D line1, Line2D line2) {
	    if (! line1.intersectsLine(line2) ) return null;
	      double px = line1.getX1(),
	            py = line1.getY1(),
	            rx = line1.getX2()-px,
	            ry = line1.getY2()-py;
	      double qx = line2.getX1(),
	            qy = line2.getY1(),
	            sx = line2.getX2()-qx,
	            sy = line2.getY2()-qy;

	      double det = sx*ry - sy*rx;
	      if (det == 0) {
	        return null;
	      } else {
	        double z = (sx*(qy-py)+sy*(px-qx))/det;
	        if (z==0 ||  z==1) return null;  // intersection at end point!
	        return new Point2D.Double(
	          (px+z*rx), (py+z*ry));
	      }
	 } // end intersection line-line
	
	/*private static void drawUCLines(Diagram diagram, Pane canvas) {
		List<Connection> connections = diagram.getConnections();
		for (int i = 0; i < connections.size(); i++) {
			Connection c = connections.get(i);
			Type type = c.getType();

			DiagramLine line = DiagramLine.ucLine(c.getStart(), c.getGoal(),
					type);
			canvas.getChildren().add(line);
		}
	}*/

	@SuppressWarnings("deprecation")
	public static Diagram drawClassDiagram(ScrollPane diagramPane,
			Diagram diagram, int cellSize, boolean order) {
		Pane canvas = new Pane();
		diagramPane.setContent(canvas);
		Random rand = new Random();
		List<ConnectableObject> conns = diagram.getConnectables();
		List<ClassControl> controls = new ArrayList<ClassControl>();
		int bestScore = Integer.MAX_VALUE;
		ClassDiagram bestDiagram = (ClassDiagram) diagram.Clone();
		for (ConnectableObject c : conns) {
			ClassBox caja = (ClassBox) c;

			ClassControl clas = new ClassControl(caja);
			canvas.getChildren().add(clas);
			clas.impl_processCSS(true);
			int roundedWidth = (int) (clas.prefWidth(-1) + cellSize - (clas
					.prefWidth(-1) % cellSize));
			int roundedHeight = (int) (clas.prefHeight(-1) + cellSize - (clas
					.prefHeight(-1) % cellSize));
			clas.setPrefWidth(roundedWidth);
			clas.setPrefHeight(roundedHeight);
			c.resize(roundedWidth, roundedHeight);
			controls.add(clas);
		}

		float width = 2000;
		float height = 2000;
		diagram.resize(width, height);
		
		if (order) {

		for (int i = 0; i < 80; i++) {
			//System.out.println("____");
			for (ConnectableObject c : conns) {
				c.moveTo(rand.nextInt(1000) + 300, rand.nextInt(1000) + 300);
			}
			DiagramUtils.fruchtermanReingold(diagram,100,100, cellSize, 1000, 0,2.3,
					20 + (conns.size() - 1) * 50,  false, false);

			DiagramUtils.moveToClosestCell(conns, cellSize);

			
			float minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
			float padding = 30;
			for (ConnectableObject c : conns) {
				minX = c.getX() < minX ? c.getX() : minX;
				minY = c.getY() < minY ? c.getY() : minY;
			}

			for (int j = 0; j < conns.size(); j++) {
				ConnectableObject c = conns.get(j);
				c.moveTo(c.getX() - minX + padding, c.getY() - minY + padding);	
			}
			
			setClassBackendLines(diagram, cellSize);

			// CHECK SCORE
			ArrayList<Path> paths = getPaths(diagram, cellSize);
			int currentScore = DiagramUtils.score(paths, 5, 1);
			if (currentScore < bestScore ) {
				bestScore = currentScore;
				bestDiagram = (ClassDiagram) diagram.Clone();
			}
		}
		}
		
		if (!order) {
			bestDiagram = (ClassDiagram) diagram;
			setClassBackendLines(bestDiagram, cellSize);
			
		}
		//Mover controls
		for (int j = 0; j < controls.size(); j++) {
			controls.get(j).relocate(
					bestDiagram.getConnectables().get(j).getX(),
					bestDiagram.getConnectables().get(j).getY());
		}
		drawClassLines(bestDiagram, cellSize, canvas);
		return bestDiagram;
	}

	private static void setClassBackendLines(Diagram diagram, int cellSize) {
		List<Connection> connections = diagram.getConnections();
		
		for (Connection c : connections) {
			
			String from = c.getFrom();
			String to = c.getTo();
			ConnectableObject obj1 = diagram.getConnById(from);
			ConnectableObject obj2 = diagram.getConnById(to);

			Point[] points = ClassBox.bestAnchors((ClassBox)obj1, (ClassBox)obj2, cellSize);

			
			c.setStart(points[0]);
			c.setGoal(points[1]);
		}
	}


	private static void drawClassLines(Diagram diagram, int cellSize,
			Pane canvas) {
		List<Connection> connections = diagram.getConnections();
		for (int i = 0; i < connections.size(); i++) {
			
			Connection c = connections.get(i);
			Type type = c.getType();
			DiagramLine line = DiagramLine.classLine((Point)c.getStart(), (Point)c.getGoal(), diagram,
					cellSize, type);
			if (line != null) canvas.getChildren().add(line);
		}
	}
	
	private static ArrayList<Path> getPaths(Diagram diagram, int cellSize) {
		ArrayList<Path> paths = new ArrayList<Path>();
		List<Connection> connections = diagram.getConnections();
		for (Connection c : connections) {
			paths.add(DiagramLine.classLinePath((Point)c.getStart(), (Point)c.getGoal(), diagram, cellSize));
		}
		return paths;
	}
	
	
	
	/*
	 * private static void drawClassLines(Diagram diagram, Pane canvas) {
	 * List<Connection> connections = diagram.getConnections(); for (int i = 0;
	 * i < connections.size(); i++) { String from =
	 * connections.get(i).getFrom(); String to = connections.get(i).getTo();
	 * ConnectableObject obj1 = diagram.getConnById(from); ConnectableObject
	 * obj2 = diagram.getConnById(to); float posX1 = (float) (obj1.getX() +
	 * obj1.getWidth() / 2); float posY1 = (float) (obj1.getY() +
	 * obj1.getHeight() / 2); float posX2 = (float) (obj2.getX() +
	 * obj2.getWidth() / 2); float posY2 = (float) (obj2.getY() +
	 * obj2.getHeight() / 2); Type tipo = connections.get(i).getType(); Line
	 * line = new Line(posX1, posY1, posX2, posY2); line.setStrokeWidth(2);
	 * Polygon pol = null; double angle = Math.atan2(posY2 - posY1, posX2 -
	 * posX1) * 180 / 3.14;
	 * 
	 * if (tipo == Connection.Type.dependency) { pol = normal;
	 * pol.setTranslateX(posX2); pol.setTranslateY(posY2);
	 * line.getStrokeDashArray().addAll(2d, 10d); } else if (tipo ==
	 * Connection.Type.aggregation) { pol = agregation; pol.setRotate((angle -
	 * 90)); pol.setTranslateX(posX2); pol.setTranslateY(posY2); } else if (tipo
	 * == Connection.Type.composition) { pol = composition;
	 * pol.setRotate((angle+90)); pol.setTranslateX(posX1);
	 * pol.setTranslateY(posY1); } else if (tipo == Connection.Type.inheritance)
	 * { pol = inheritance; pol.setRotate((angle-90)); pol.setTranslateX(posX2);
	 * pol.setTranslateY(posY2); }
	 * 
	 * canvas.getChildren().add(line); if (pol != null)
	 * canvas.getChildren().add(pol);
	 * 
	 * } }
	 */

}
