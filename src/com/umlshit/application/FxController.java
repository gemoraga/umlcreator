package com.umlshit.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.ResourceBundle;

import com.umlshit.backend.Diagram;
import com.umlshit.backend.Class.ClassDiagram;
import com.umlshit.backend.UC.UCDiagram;
import com.umlshit.xml.XMLToDiagram;
import com.umlshit.backend.util.FileUtils;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;

public class FxController implements Initializable {

	@FXML
	private TextArea xmlUCEditor;
	@FXML
	private TextArea xmlClassEditor;
	@FXML
	private Button btnGenerateUC;
	@FXML
	private Button btnGenerateClass;
	@FXML
	private MenuBar menu;
	@FXML
	private MenuItem loadCD;
	@FXML
	private MenuItem loadUC;

	private Stage showStage;
	private Stage s;
	private Popup popup;
	
	@Override
	// This method is called by the FXMLLoader when initialization is complete
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		assert xmlUCEditor != null : "fx:id=\"xmlUCEditor\" was not injected: check your FXML file 'FxController.fxml'.";
		assert xmlClassEditor != null : "fx:id=\"xmlClassEditor\" was not injected: check your FXML file 'FxController.fxml'.";
		showStage = new Stage();
		popup = new Popup();
		VBox aboutBox = new VBox();
		aboutBox.setStyle("-fx-background-color: linear-gradient("
				+ "to bottom, "
				+ "derive(-fx-color,34%) 0%, "
				+ "derive(-fx-color,-18%) 100% );"
				+ "-fx-border-color:black");
		aboutBox.setPrefSize(200, 100);
		aboutBox.setAlignment(Pos.CENTER);
		Label about = new Label();
		about.setText("UML Editor: \n\n"
				+ "Vicente Dragicevic\n"
				+ "Gonzalo Moraga\n"
				+ "Pablo Samaja");
		aboutBox.getChildren().add(about);
		aboutBox.addEventHandler(MouseEvent.MOUSE_CLICKED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						if (e.getButton() == MouseButton.PRIMARY)
							popup.hide();
					}
				});
		popup.getContent().add(aboutBox);
		popup.centerOnScreen();
		//Styles
		//ClassControl.getStyleClass().add("classcontrol");
	}
	
	public void setStage(Stage s) {
		this.s = s; 
	}

	@FXML
	private void handleGenerateUCClick() {
		String xml = xmlUCEditor.getText();
		Diagram d = XMLToDiagram.toDiagram(xml);
		try {
			showStage.close();
			showStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ShowDiagram.fxml"));
			Pane page = (Pane) loader.load();
			ShowDiagramController controller = (ShowDiagramController)loader.getController();
			controller.setStage(showStage);
			controller.setXml(xml);
			Scene scene = new Scene(page);
			scene.getStylesheets().add("com/umlshit/application/controlStyle.css");
			showStage.setScene(scene);
			showStage.setTitle("UML Editor");
			showStage.centerOnScreen();
			showStage.show();
			ScrollPane canvas = (ScrollPane)showStage.getScene().lookup("#diagramPane");
			canvas.setStyle("-fx-background:white");
			d = DrawDiagram.drawUCDiagram(canvas, d, true);
			controller.setDiagram(d);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out
					.println("Error en el Diagrama, revisar que las conecciones apunten a objetos existentes.");
			e.printStackTrace();
		}
	}
	@FXML
	private void handleGenerateClassClick() {
		String xml = xmlClassEditor.getText();
		Diagram d = XMLToDiagram.toDiagram(xml);
		try {
			showStage.close();
			showStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("ShowDiagram.fxml"));
			Pane page = (Pane) loader.load();
			ShowDiagramController controller = (ShowDiagramController)loader.getController();
			controller.setStage(showStage);
			
			controller.setXml(xml);

			Scene scene = new Scene(page);
			scene.getStylesheets().add("com/umlshit/application/controlStyle.css");
			showStage.setScene(scene);
			showStage.setTitle("UML Editor");
			showStage.centerOnScreen();
			showStage.show();
			ScrollPane canvas = (ScrollPane)showStage.getScene().lookup("#diagramPane");
			canvas.setStyle("-fx-background:white");
			d = DrawDiagram.drawClassDiagram(canvas, d, 10, true);
			controller.setDiagram(d);
			
		} catch (Exception e) {
			System.out
					.println("Error en el Diagrama, revisar que las conecciones apunten a objetos existentes.");
			e.printStackTrace();
		}
	}

	@FXML
	private void handleLoadUCClick() throws IOException {
		String content = "";
		File selected = FileUtils.OpenFile(s, "TXT Files (*.txt)", "*.txt");
		if (selected != null) {
			xmlUCEditor.clear();
			FileReader fr;
			fr = new FileReader(selected);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			while ((linea = br.readLine()) != null) {
				content += linea + "\n";
			}
			xmlUCEditor.setText(content);
			br.close();
		} else {
			System.out.println("Elija un archivo.");
		}
	}

	@FXML
	private void handleLoadCDClick() throws IOException {
		String content = "";
		File selected = FileUtils.OpenFile(s, "TXT Files (*.txt)", "*.txt");
		if (selected != null) {
			xmlClassEditor.clear();
			FileReader fr;
			fr = new FileReader(selected);
			BufferedReader br = new BufferedReader(fr);
			String linea;
			while ((linea = br.readLine()) != null) {
				content += linea + "\n";
			}
			xmlClassEditor.setText(content);
			br.close();
		} else {
			System.out.println("Elija un archivo.");
		}

	}
	
	@FXML
	private void loadFromFile(){
		
		Diagram d;
		File selected = FileUtils.OpenFile(s, "UML Files (*.uml)", "*.uml");
		if (selected ==null) {System.out.println("Elija un archivo.");return;}
		List<String> file;
		try {
			file = Files.readAllLines(selected.toPath());
		
		String xml = file.get(0);
		String[] objects = file.get(1).split(";");
		
		String[] notes = null;
		if (file.size() > 2) notes = file.get(2).split(";");
		d = XMLToDiagram.toDiagram(xml);
		if (d.getClass() == UCDiagram.class) {
			xmlUCEditor.setText(xml);
		} else {
			xmlClassEditor.setText(xml);
		}
		
		for (String obj : objects) {
			String[] elements = obj.split(":");
			String id = elements[0];
			String[] coords = elements[1].split(",");
			d.getConnById(id).moveTo(Float.valueOf(coords[0]),
					Float.valueOf(coords[1]));
		}

		showStage.close();
		showStage = new Stage();

		FXMLLoader loader = new FXMLLoader(getClass().getResource(
				"ShowDiagram.fxml"));
		Pane page = (Pane) loader.load();
		ShowDiagramController controller = (ShowDiagramController) loader
				.getController();
		controller.setStage(showStage);
		controller.setXml(xml);

		Scene scene = new Scene(page);
		scene.getStylesheets().add("com/umlshit/application/controlStyle.css");
		showStage.setScene(scene);
		showStage.setTitle("UML Editor");
		showStage.centerOnScreen();
		showStage.show();
		ScrollPane canvas = (ScrollPane) showStage.getScene().lookup(
				"#diagramPane");
		canvas.setStyle("-fx-background:white");

		if (d.getClass().equals(ClassDiagram.class))
			d = DrawDiagram.drawClassDiagram(canvas, d, 10, false);
		else
			d = DrawDiagram.drawUCDiagram(canvas, d, false);
		controller.setDiagram(d);

		// NOTAS
		if (notes != null && notes.length != 0) {
			
			for (String note : notes) {
				String[] elements = note.split(":");
				String text = elements[0].replace("\\w", "\n");
				String[] coords = elements[1].split(",");
				controller.newNote(Float.valueOf(coords[0]),
						Float.valueOf(coords[1]), text);
			}
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@FXML
	private void aboutPopup() {
		popup.show(s);
	}
	
}
