package com.umlshit.application.customControls;
import java.io.IOException;

import com.umlshit.backend.Class.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
public class ClassControl extends VBox {
	
	    @FXML private GridPane grid;
	    
	    int currentRow;
	    
	    public ClassControl(ClassBox clase) {
	    	currentRow = 0;
	    	//getStylesheets().add("com/umlshit/application/controlStyle.css");
	    	getStyleClass().add("classcontrol");
	        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ClassControl.fxml"));
	        fxmlLoader.setRoot(this);
	        fxmlLoader.setController(this);
	        Font title = Font.font("Verdana", FontWeight.BOLD, 16);
	        
	        Font attribute = Font.font("Verdana", FontWeight.BOLD, 12);//Font.loadFont("Verdana", 12);
	        Font method = Font.font("Verdana", FontWeight.BOLD, 12);
	        try {
	            fxmlLoader.load();
	            ColumnConstraints col1 = new ColumnConstraints();
	            col1.setPercentWidth(100);
	            grid.getColumnConstraints().add(col1);
	            
	            addLabel(" " + clase.getName(), title, "-fx-background-color: linear-gradient(#9dbfbe, #8aa8a7);", "-fx-text-fill: #ffffff");
	            
	            addSeparator("");
	            for (int i = 0; i<clase.getAttributes().size();i++){
	            
	            String atributoAagregar = clase.getAttributes().get(i).toString();
	            addLabel(" " + atributoAagregar, attribute, "","-fx-text-fill: #728c8b");
	            }
	            addSeparator("dashed");
	            for (int i = 0; i<clase.getMethods().size();i++){
		            
		            String metodoAagregar = clase.getMethods().get(i).toString();
		            addLabel(" " + metodoAagregar, method, "","-fx-text-fill: #728c8b");
		            }
	            
	        } catch (IOException exception) {
	            throw new RuntimeException(exception);
	        }
	        
	        
	    }
	    

	    public void addLabel(String text, Font font, String boxStyle, String labelStyle) {
	    	VBox box = new VBox();
	    	box.setStyle(boxStyle);
	    	Label lbl = new Label();
	    	lbl.setStyle(labelStyle);
	    	GridPane.setFillWidth(box, true);
	    	//lbl.setPrefWidth(Double.MAX_VALUE);
	    	lbl.setText(text);
	    	lbl.setFont(font);
	    	
	    	box.getChildren().add(lbl);
	    	GridPane.setColumnIndex(box, 0);
	    	GridPane.setRowIndex(box, currentRow);
	    	grid.getChildren().add(box);
	    	currentRow++;
	    }
	    
	    public void addSeparator(String id) {
	    	Separator sep = new Separator();
	    	if (!id.equals(""))	sep.setId(id);
	    	sep.setOrientation(Orientation.HORIZONTAL);
	        sep.setValignment(VPos.CENTER);
	        //sep.setPrefHeight(80);
	        GridPane.setConstraints(sep, 0, currentRow);
	        grid.getChildren().add(sep);
	        currentRow++;
	    }
	    
	        
	   
	}