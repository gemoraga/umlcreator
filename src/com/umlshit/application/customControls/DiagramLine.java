package com.umlshit.application.customControls;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.umlshit.backend.Connection;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.util.*;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

public class DiagramLine extends Group{
	
	static Double[] normal, composition, inheritance, aggregation;
	static {
		normal = new Double[] { -10.0, -5.0, 0.0, 5.0, 10.0, -5.0, 0.0, 5.0 };
		
		composition = new Double[] { 0.0, 5.0, -5.0, -5.0, 0.0, -15.0, 5.0, -5.0 };
		
		inheritance = new Double[] { -10.0, -5.0, 0.0, 5.0, 10.0, -5.0};
		
		aggregation = new Double[] { 0.0, 5.0, -5.0, -5.0, 0.0, -15.0, 5.0, -5.0 };

	}
	
	public static DiagramLine ucLine(Point2D start, Point2D goal, Connection.Type type) {
		DiagramLine dl = new DiagramLine();
		double X1 = start.getX(), Y1 = start.getY(), X2 = goal.getX(), Y2 = goal.getY();
		
		Polygon p = null;
		Text tb = null;
		double angle = Math.atan2(Y2 - Y1, X2 - X1) * 180 / Math.PI;
		Line line = new Line(X1, Y1, X2, Y2);
		line.setStrokeWidth(1.2);

		switch (type) {
		case extend: 
			tb = new Text((X2 + X1) / 2, (Y2 + Y1)/2, "<<extend>>");
			line.getStrokeDashArray().addAll(2d, 10d);
			p = new Polygon();
			p.setStroke(Color.BLACK);
			p.getPoints().addAll(normal);
			break;
		case include:
			tb = new Text((X2 + X1) / 2, (Y2 + Y1)/2, "<<include>>");
			line.getStrokeDashArray().addAll(2d, 10d);
			p = new Polygon();
			p.setStroke(Color.BLACK);
			p.getPoints().addAll(normal);
			break;
		case isa:
			p = new Polygon();
			p.setStroke(Color.BLACK);
			p.getPoints().addAll(normal);
			break;
		}
		
		dl.getChildren().add(line);
		if (tb != null)	dl.getChildren().add(tb);
		if (p != null) {
			p.setTranslateX(X2);
			p.setTranslateY(Y2);
			p.getTransforms().add(new Rotate(angle - 90));

			dl.getChildren().add(p);
		}		
		return dl;
	}
	
	public static DiagramLine classLine(Point startingPoint, Point goalPoint, Diagram d, int cellSize, Connection.Type type) {
		DiagramLine dl = new DiagramLine();
		Polygon p = null;
		Path path = classLinePath(startingPoint, goalPoint, d, cellSize);
		//Necesitamos ver si el último tramo del path fue horizontal o vertical
		if (path == null || path.getElements().size() < 2) return null;
		double angle = getEndAngle(path);

		switch (type) {
		case dependency:
			p = new Polygon();
			p.getPoints().addAll(normal);
			p.setFill(Color.TRANSPARENT);
			p.setStroke(Color.BLACK);
			p.getTransforms().add(new Rotate(angle));
			path.getStrokeDashArray().addAll(2d, 10d);
			break;
			
		case aggregation:
			p = new Polygon();
			p.getPoints().addAll(aggregation);
			p.setFill(Color.WHITE);
			p.setStroke(Color.BLACK);
			p.getTransforms().add(new Rotate(angle));
			break;
			
		case composition:
			p = new Polygon();
			p.getPoints().addAll(composition);
			p.getTransforms().add(new Rotate(angle));
			break;
			
		case inheritance:
			p = new Polygon();
			p.getPoints().addAll(inheritance);
			p.setFill(Color.WHITE);
			p.setStroke(Color.BLACK);
			p.getTransforms().add(new Rotate(angle));
			
			break;
		}
		dl.getChildren().add(path);
		if (p != null) {
			dl.getChildren().add(p);
			LineTo last = (LineTo)path.getElements().get(path.getElements().size()-1);
			Point2D g = new Point2D.Double(last.getX(), last.getY());
			setUpPolygonAtEnd(p, angle, g, cellSize);
			
		}
		return dl;
	}
	
	private static void setUpPolygonAtEnd(Polygon p, double angle, Point2D last, int cellSize) {
		double dx = last.getX(), dy = last.getY(); 
		if (angle == 0) {//abajo
			dy -= cellSize/2;
		} else if (angle == -180) {//arriba
			dy += cellSize/2;
		} else if (angle == 90) {//izquierda
			dx += cellSize/2;
		} else if (angle == -90) { //derecha
			dx -= cellSize/2;
		}
		p.setTranslateX(dx);
		p.setTranslateY(dy);
	}
	
	public static Path classLinePath(Point startingPoint, Point goalPoint, Diagram d, int cellSize) {
		float width = d.getWidth();
		float height = d.getHeight();
		Path path = new Path();
		
		AreaMap area = new AreaMap((int)(width / cellSize), (int)(height / cellSize), d, cellSize);
		
		//System.out.print(area.toString());
		AStar astar = new AStar(area, new heuristic());
		ArrayList<Point> points = astar.calcShortestPath(startingPoint.x/cellSize, startingPoint.y/cellSize, goalPoint.x/cellSize, goalPoint.y/cellSize);
		
		//System.out.println(startingPoint.toString() + ", " + goalPoint.toString());
		if (points != null) {
			for (int i = 0; i < points.size(); i++){
				Point p = points.get(i);
				if (i == 0){
					double angle = getStartAngle(points);
					double dx = p.getX()*cellSize + cellSize/2, dy = p.getY()*cellSize + cellSize/2;
					
					if (angle == 0) {//abajo
						dy -= cellSize/2;
					} else if (angle == -180) {//arriba
						dy += cellSize/2;
					} else if (angle == 90) {//izquierda
						dx += cellSize/2;
					} else if (angle == -90) { //derecha
						dx -= cellSize/2;
					}
					MoveTo start = new MoveTo(dx, dy);
					path.getElements().add(start);
				}
				else if(i == points.size() - 1) {
					double angle = getEndAngle(points);
					double dx = p.getX()*cellSize + cellSize/2, dy = p.getY()*cellSize + cellSize/2;
					
					if (angle == 0) {//abajo
						dy -= cellSize/2;
					} else if (angle == -180) {//arriba
						dy += cellSize/2;
					} else if (angle == 90) {//izquierda
						dx += cellSize/2;
					} else if (angle == -90) { //derecha
						dx -= cellSize/2;
					}
					LineTo start = new LineTo(dx, dy);
					path.getElements().add(start);
				} 
				else {
					path.getElements().add(new LineTo(p.getX()*cellSize + cellSize/2, p.getY()*cellSize + cellSize/2));
				}
			}
		}
		return path;
	}
	
	private static double getEndAngle(Path path){
		double a1X  = ((LineTo)path.getElements().get(path.getElements().size() -2)).getX();
		double a1Y  = ((LineTo)path.getElements().get(path.getElements().size() -2)).getY();
		double a2X  = ((LineTo)path.getElements().get(path.getElements().size() -1)).getX();
		double a2Y  = ((LineTo)path.getElements().get(path.getElements().size() -1)).getY();
		double dx = a2X - a1X, dy = a2Y - a1Y;
		return Math.atan2(dy, dx) * 180.0 / Math.PI - 90;
	}
	
	private static double getEndAngle(ArrayList<Point> points){
		double a1X  = points.get(points.size() - 2).getX();
		double a1Y  = points.get(points.size() - 2).getY();
		double a2X  = points.get(points.size() - 1).getX();
		double a2Y  = points.get(points.size() - 1).getY();
		double dx = a2X - a1X, dy = a2Y - a1Y;
		return Math.atan2(dy, dx) * 180.0 / Math.PI - 90;
	}
	
	private static double getStartAngle(Path path){
		double a1X  = ((MoveTo)path.getElements().get(0)).getX();
		double a1Y  = ((MoveTo)path.getElements().get(0)).getY();
		double a2X  = ((LineTo)path.getElements().get(1)).getX();
		double a2Y  = ((LineTo)path.getElements().get(1)).getY();
		double dx = a2X - a1X, dy = a2Y - a1Y;
		return Math.atan2(dy, dx) * 180.0 / Math.PI - 90;
	}
	
	private static double getStartAngle(ArrayList<Point> points){
		double a1X  = points.get(0).getX();
		double a1Y  = points.get(0).getY();
		double a2X  = points.get(1).getX();
		double a2Y  = points.get(1).getY();
		double dx = a2X - a1X, dy = a2Y - a1Y;
		return Math.atan2(dy, dx) * 180.0 / Math.PI - 90;
	}
	
}
