package com.umlshit.application.customControls;

import com.umlshit.backend.*;

import java.io.IOException;
import java.util.Random;

import com.umlshit.backend.UC.*;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
public class ActorControl extends VBox {
	

	    @FXML private Label title;
	    @FXML private ImageView image;
	    
	    private Actor actor;
	    public ActorControl(Actor actor) {
	    	//getStylesheets().add("com/umlshit/application/controlStyle.css");
	    	getStyleClass().add("actorcontrol");
	    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ActorControl.fxml"));
	        fxmlLoader.setRoot(this);
	        fxmlLoader.setController(this);
	        this.actor= actor;
	        Random rand = new Random();
	        try {
	            fxmlLoader.load();
	            image.setImage(new Image("Resources/png/a" + rand.nextInt(9) + ".png"));
	            setAlignment(Pos.CENTER);
	        } catch (IOException exception) {
	            throw new RuntimeException(exception);
	        }
	    }
	    
	    public ConnectableObject getBackend(){
	    	return actor;
	    }
	    public void setTitle() {
	        titleTextProperty().set(actor.getName());
	    }
	    
	    public StringProperty titleTextProperty() {
	        return title.textProperty();                
	    }
	   
	}

