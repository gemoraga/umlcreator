package com.umlshit.application.customControls;

import com.umlshit.backend.*;

import java.io.IOException;

import com.umlshit.backend.UC.*;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;

public class UseCaseControl extends VBox{
    @FXML private Label title;
    private UCBox ucbox;
    public UseCaseControl(UCBox ucbox) {
    	//getStylesheets().add("com/umlshit/application/controlStyle.css");
    	getStyleClass().add("usecasecontrol");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("UseCaseControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        this.ucbox= ucbox;
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
    
    public ConnectableObject getBackend(){
    return ucbox;
    }
    
    public void setTitle() {
        titleTextProperty().set(ucbox.getName());
    }
    
    public StringProperty titleTextProperty() {
        return title.textProperty();                
    }
    
        
   
}
