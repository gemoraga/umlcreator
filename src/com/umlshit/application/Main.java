package com.umlshit.application;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class Main extends Application {

	public static void main(String[] args) throws IOException {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        try {
        	FXMLLoader loader = new FXMLLoader(getClass().getResource("MainFx.fxml"));
        	Pane page = (Pane)loader.load();
        	FxController controller = (FxController)loader.getController();
        	controller.setStage(primaryStage);
            Scene scene = new Scene(page);
            //scene.getStylesheets().add("com/umlshit/application/controlStyle.css");
            primaryStage.setScene(scene);
            primaryStage.setTitle("UML Editor");
            primaryStage.centerOnScreen();
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
