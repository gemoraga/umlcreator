package com.umlshit.testing;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.umlshit.application.DrawDiagram;
import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.UC.Actor;
import com.umlshit.backend.UC.UCBox;
import com.umlshit.backend.UC.UCDiagram;

import javafx.scene.control.ScrollPane;

@RunWith(JfxRunner.class)
public class UCTest {
	UCDiagram diagram;
	
	//ACA SE CREA EL DIAGRAMA RANDOM
	@Before
	public void init() {
		diagram = new UCDiagram("Diagrama");
		Random rand = new Random();
		int actors = rand.nextInt(3)+3;
		int ucs = rand.nextInt(5) + 5;
		int connections = rand.nextInt(5)+ 3;
		for (int i = 0; i < actors; i++) {
			diagram.addActor(new Actor("a" + i, "Actor" + i, rand.nextBoolean()?"Primary":"Secondary"));
		}
		for (int i = 0; i < ucs; i++) {
			diagram.addBox(new UCBox("u" + i, "UseCase" + i));
		}
		
		for (int i = 0; i < connections; i++) {
			String from = rand.nextBoolean()?"a"+rand.nextInt(actors):"u"+rand.nextInt(ucs);
			String to = rand.nextBoolean()?"a" + rand.nextInt(actors):"u" +rand.nextInt(ucs);
			Connection c = new Connection(from, to);
			c.setType(Connection.Type.values()[1]);
			diagram.addConnection(c);
		}
	}
	
	@Test (timeout = 10000)
	public void test() {
		DrawDiagram.drawUCDiagram(new ScrollPane(), diagram, true);
		
	}
	
	@Test
	public void testCajasSeVenEnDiagrama(){
		DrawDiagram.drawUCDiagram(new ScrollPane(), diagram, true);
		for (ConnectableObject c: diagram.getConnectables()){
			assertTrue("Las cajas NO entran en el diagrama...", c.getX()>0 && c.getY()>0);
		}
	}

}
