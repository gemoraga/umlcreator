package com.umlshit.testing;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.umlshit.application.DrawDiagram;
import com.umlshit.application.ShowDiagramController;
import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.Class.Attribute;
import com.umlshit.backend.Class.ClassBox;
import com.umlshit.backend.Class.ClassDiagram;
import com.umlshit.backend.Class.Method;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
@RunWith(JfxRunner.class)
public class noteTest {

	ShowDiagramController controller;
	@Before
	public void init() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/umlshit/application/ShowDiagram.fxml"));
		try {
			loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		controller = (ShowDiagramController)loader.getController();
		controller.diagramPane.setContent(new Pane());
	}
	@Test
	public void testNote() {
		int hijosAntes= controller.diagramPane.getChildrenUnmodifiable().size();
		controller.newNote(0, 0, "testing");
		int hijosDespues = controller.diagramPane.getChildrenUnmodifiable().size();
		assertTrue("No cambio el tamaño", hijosAntes==hijosDespues);
	}

}
