package com.umlshit.testing;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.umlshit.application.DrawDiagram;
import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.Class.Attribute;
import com.umlshit.backend.Class.ClassBox;
import com.umlshit.backend.Class.ClassDiagram;
import com.umlshit.backend.Class.Method;

import javafx.scene.control.ScrollPane;

@RunWith(JfxRunner.class)
public class ClassTest {
	ClassDiagram diagram;
	
	//ACA SE CREA EL DIAGRAMA RANDOM
	@Before
	public void init() {
		diagram = new ClassDiagram("Diagrama");
		Random rand = new Random();
		int boxes = rand.nextInt(5) + 5;
		int connections = rand.nextInt(5)+ 5;
		
		for (int i = 0; i < boxes; i++) {
			ClassBox cbox = new ClassBox("c" + i, "Class" + i);
			for (int j = 0; j < rand.nextInt(2); j++){
				cbox.addAttribute(new Attribute("Atributo", "TIPO", ClassDiagram.visibility.values()[rand.nextInt(3)]));
			}
			for (int j = 0; j < rand.nextInt(2); j++){
				cbox.addMethod(new Method("Metodo", "TIPO", ClassDiagram.visibility.values()[rand.nextInt(3)]));
			}
			diagram.addBox(cbox);
		}
		
		for (int i = 0; i < connections; i++) {
			Connection c = new Connection("c"+rand.nextInt(boxes), "c"+rand.nextInt(boxes));
			c.setType(Connection.Type.values()[5]);
			diagram.addConnection(c);
		}
	}
	
	@Test (timeout = 10000)
	public void testTime() {
		
		DrawDiagram.drawClassDiagram(new ScrollPane(), diagram,10, true);

	}
	@Test
	public void testCajasSeVenEnDiagrama(){
		DrawDiagram.drawClassDiagram(new ScrollPane(), diagram,10, true);
		for (ConnectableObject c: diagram.getConnectables()){
			assertTrue("Las cajas NO entran en el diagrama...", c.getX()>0 && c.getY()>0);
		}
	}

}
