package com.umlshit.testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ClassTest.class, UCTest.class, noteTest.class })
public class AllTests {

}
