package com.umlshit.xml;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.umlshit.backend.Connection;
import com.umlshit.backend.Connection.Type;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.Class.ClassDiagram;
import com.umlshit.backend.Class.Attribute;
import com.umlshit.backend.Class.Method;
import com.umlshit.backend.Class.Parameter;
import com.umlshit.backend.Class.ClassBox;
import com.umlshit.backend.UC.Actor;
import com.umlshit.backend.UC.UCBox;
import com.umlshit.backend.UC.UCDiagram;

public final class XMLToDiagram {

	private static Document Parse(String xml) {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(
					new ByteArrayInputStream(xml.getBytes("utf-8"))));
			doc.normalize();
			return doc;

		} catch (Exception e) {
			System.out.println("Error en el xml, revíselo.");
		}

		return null;
	}

	public static Diagram toDiagram(String xml) {
		try {
			Document doc = Parse(xml);
			Element root = doc.getDocumentElement();
			String name = root.getNodeName();
			if (name.equals("ClassDiagram"))
				return toClassDiagram(xml);
			else if (name.equals("UseCaseDiagram"))
				return toUCDiagram(xml);
			else
				System.out
						.println("El documento debe empezar con \"ClassDiagram\" o \"UCDiagram\"");
		} catch (Exception e) {
			System.out.println("Error en el xml");
		}
		return null;
	}

	public static Diagram toClassDiagram(String xml) {
		try {
			Document doc = Parse(xml);
			Element root = doc.getDocumentElement();

			ClassDiagram cDiagram = new ClassDiagram(root.getAttribute("name"));

			Iterator<String[]> classIt;
			Iterator<String[]> attIt;
			Iterator<String[]> methodIt;
			Iterator<String[]> paramIt;
			NodeList classboxes = root.getElementsByTagName("class");
			classIt = extractElements(classboxes, "id", "name", "type").iterator();
			//Clases
			for (int i = 0; i < classboxes.getLength(); i++) {
				String[] clase = classIt.next();
				ClassBox cbox = new ClassBox(clase[0], clase[1]);
				Element raiz = (Element) classboxes.item(i);
				NodeList atributos = raiz.getElementsByTagName("att");
				
				
				//Atributos
				attIt = extractElements(atributos, "name", "type", "visibility")
						.iterator();
				while (attIt.hasNext()) {
					String[] atributo = attIt.next();
					ClassDiagram.visibility visibilidad = ClassDiagram.visibility.Private;
					switch (atributo[2]) {
					case "-":
						visibilidad = ClassDiagram.visibility.Private;
						break;
					case "+":
						visibilidad = ClassDiagram.visibility.Public;
						break;
					case "#":
						visibilidad = ClassDiagram.visibility.Protected;
						break;
					}
					Attribute a = new Attribute(atributo[0], atributo[1], visibilidad);
					a.toString();
					cbox.addAttribute(a);
				}
				
				//Métodos
				int methodCount = 0;
				NodeList metodos = raiz.getElementsByTagName("method");
				methodIt = extractElements(metodos, "name", "type", "visibility")
						.iterator();
				while (methodIt.hasNext()) {
					String[] metodo = methodIt.next();
					ClassDiagram.visibility visibilidad = ClassDiagram.visibility.Private;
					switch (metodo[2]) {
					case "-":
						visibilidad = ClassDiagram.visibility.Private;
					case "+":
						visibilidad = ClassDiagram.visibility.Public;

					case "#":
						visibilidad = ClassDiagram.visibility.Protected;
					}
					Method m = new Method(metodo[0], metodo[1], visibilidad);
					Element metodoRaiz = (Element) metodos.item(methodCount);
					
					//Parametros
					NodeList parametros = metodoRaiz
							.getElementsByTagName("param");
					paramIt = extractElements(parametros, "name", "type")
							.iterator();
					while (paramIt.hasNext()) {
						String[] param = paramIt.next();
						Parameter p = new Parameter(param[0], param[1]);
						m.addParameter(p);
					}
					cbox.addMethod(m);
					methodCount++;
				}
				cDiagram.addBox(cbox);
			}
			//Conecciones
			Iterator<String[]> conIt;
			NodeList connections = root.getElementsByTagName("connection");
			conIt = extractElements(connections, "from", "to", "type")
					.iterator();
			while (conIt.hasNext()) {
				String[] connection = conIt.next();
				Connection c = new Connection(connection[0], connection[1]);
				switch (connection[2]) {
				case "dependency":
					c.setType(Type.dependency);
					break;
				case "association":
					c.setType(Type.association);
					break;
				case "aggregation":
					c.setType(Type.aggregation);
					break;
				case "composition":
					c.setType(Type.composition);
					break;
				case "inheritance":
					c.setType(Type.inheritance);
					break;
				}
				cDiagram.addConnection(c);
			}
			return cDiagram;
		} catch (Exception e) {
			System.out.println("Error en el xml");
		}
		return null;
	}

	public static Diagram toUCDiagram(String xml) {
		try {
			Document doc = Parse(xml);
			Element root = doc.getDocumentElement();

			UCDiagram ucDiagram = new UCDiagram(root.getAttribute("name"));

			Iterator<String[]> it;
			NodeList actors = root.getElementsByTagName("actor");
			it = extractElements(actors, "id", "name", "type").iterator();
			while (it.hasNext()) {
				String[] s = it.next();
				ucDiagram.addActor(new Actor(s[0], s[1], s[2]));
			}

			NodeList useCases = root.getElementsByTagName("usecase");
			it = extractElements(useCases, "id", "name").iterator();
			while (it.hasNext()) {
				String[] s = it.next();
				ucDiagram.addBox(new UCBox(s[0], s[1]));
			}

			NodeList connections = root.getElementsByTagName("connection");
			it = extractElements(connections, "from", "to", "type").iterator();
			while (it.hasNext()) {
				String[] s = it.next();
				Connection c = new Connection(s[0], s[1]);
				switch (s[2]) {
				case "basic":
					c.setType(Type.basic);
					break;
				case "extend":
					c.setType(Type.extend);
					break;
				case "include":
					c.setType(Type.include);
					break;
				case "isa":
					c.setType(Type.isa);
					break;
				}
				ucDiagram.addConnection(c);
			}
			return ucDiagram;
		} catch (Exception e) {
			System.out.println("Error en el xml");
		}
		return null;
	}

	private static List<String[]> extractElements(NodeList list,
			String... attributes) {
		Node node = list.item(0);
		List<String[]> elements = new ArrayList<String[]>();
		while (node != null) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				String[] atts = new String[attributes.length];
				for (int i = 0; i < atts.length; i++) {
					atts[i] = ((Element) node).getAttribute(attributes[i]);
				}
				elements.add(atts);
			}
			node = node.getNextSibling();
		}
		return elements;
	}

}
