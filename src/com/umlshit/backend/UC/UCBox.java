package com.umlshit.backend.UC;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

import com.umlshit.backend.Box;
import com.umlshit.backend.ConnectableObject;

public class UCBox extends Box {

	public UCBox(String id, String name) {
		super(id, name);
	}
	
	private UCBox(String id, String name, float x, float y, double width, double height){
		super(id, name, x, y, width, height);
	}
	
	@Override public ConnectableObject Clone(){
		return new UCBox(this.getId(),this.name, this.getX(), this.getY(), this.getWidth(), this.getHeight());
	}
	
	

}
