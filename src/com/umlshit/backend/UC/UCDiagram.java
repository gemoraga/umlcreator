package com.umlshit.backend.UC;

import java.util.ArrayList;
import java.util.List;

import com.umlshit.backend.Box;
import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.Diagram;

public class UCDiagram extends Diagram{
	private List<Actor> actors;
	
	public UCDiagram(String name) {
		super(name);
		actors = new ArrayList<Actor>();
	}
	
	public void addActor(Actor actor){
		actors.add(actor);
	}
	
	public List<Actor> getActors(){
		return actors;
	}
	
	@Override public String toString() {
		StringBuilder result = new StringBuilder();
	    String NEW_LINE = System.getProperty("line.separator");
	    
	    result.append("Diagram Name: " + this.name + NEW_LINE);
	    result.append(" Actors:" + NEW_LINE);
	    for (int i = 0; i < actors.size(); i++) {
	    	Actor actor = actors.get(i);
	    	result.append("  Name: " + actor.getName() + ", type: " + actor.getType() + NEW_LINE);
	    }
	    
	    result.append(" Use Cases:" + NEW_LINE);
	    for (int i = 0; i < boxes.size(); i++) {
	    	UCBox usecase = (UCBox) boxes.get(i);
	    	result.append("  Name: " + usecase.getName() + NEW_LINE);
	    }
	    
	    result.append(" Connections:" + NEW_LINE);
	    for (int i = 0; i < connections.size(); i++) {
	    	Connection connection = connections.get(i);
	    	result.append("  From: " + connection.getFrom() + ", To: " + connection.getTo() + NEW_LINE);
	    }
	    
		return result.toString();
	}
	
	@Override public List<ConnectableObject> getConnectables(){
		List<ConnectableObject> connectables = new ArrayList<ConnectableObject>();
		connectables.addAll(boxes);
		connectables.addAll(actors);
		return connectables;
	}

	@Override
	public Diagram Clone() {
		UCDiagram clone = new UCDiagram(this.name);
		for (Box b : this.boxes) {
			clone.boxes.add((Box) b.Clone());
		}
		for (Actor a : this.actors) {
			clone.actors.add((Actor) a.Clone());
		}
		for (Connection c: this.connections) {
			clone.connections.add(c.Clone());
		}
		return clone;
	}
}
