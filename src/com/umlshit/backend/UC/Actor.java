package com.umlshit.backend.UC;

import com.umlshit.backend.ConnectableObject;

public class Actor extends ConnectableObject{
	private String name;
	private String type;
	
	public Actor(String id, String name, String type) {
		super(id);
		this.name = name;
		this.type = type;
		fixedPosition = true;
	}
	
	private Actor(String id, String name, String type,  float x, float y, double width, double height) {
		super(id, x, y, width, height);
		this.name = name;
		this.type = type;
		fixedPosition = true;
	}
	
	
	public String getName(){
		return name;
	}
	
	public String getType(){
		return type;
	}
	
	@Override public ConnectableObject Clone(){
		return new Actor(this.getId(),this.name, this.type, this.getX(), this.getY(), this.getWidth(), this.getHeight());
	}
	
}
