package com.umlshit.backend.util;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.shape.Path;

import com.umlshit.backend.ConnectableObject;
import com.umlshit.backend.Connection;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.Force;

public class DiagramUtils {

	// Properties
	private static float area;
	private static double coolExp = 2.5;

	public static void fruchtermanReingold(Diagram diagram, float rep, float att, int cellSize, int iterations, float xMargin, double gravity, float gravityRadius, boolean overlap,
			boolean noRepulsion) {
		List<ConnectableObject> connList = diagram.getConnectables();
		List<Connection> connectList = diagram.getConnections();
		ConnectableObject[] connectables = connList
				.toArray(new ConnectableObject[connList.size()]);
		Connection[] connections = connectList
				.toArray(new Connection[connectList.size()]);

		float width = diagram.getWidth();
		float height = diagram.getHeight();
		// area = width * height ;
		area = (float) Math.pow(connectables.length, 6);
		float maxDisplace = (float) (connectables.length);
		float k = (float) Math.sqrt(area / (float) connectables.length);

		for (int i = iterations; i > 0; i--) {
			for (ConnectableObject c : connectables) {
				Force cForce = c.getForce();
				cForce.dx = 0;
				cForce.dy = 0;
			}

			// Attraction
			for (Connection conn : connections) {
				ConnectableObject from = diagram.getConnById(conn.getFrom());
				ConnectableObject to = diagram.getConnById(conn.getTo());
				attraction_noCollition(from, to, att / (1 + diagram.getDegreeOf(from)), overlap);
			}

			for (int m = 0; m < connectables.length; m++) {
				for (int n = m + 1; n < connectables.length; n++) {
					ConnectableObject c1 = connectables[m], c2 = connectables[n];
					if (c1 == c2 || c1.isFixed() || c2.isFixed())
						continue;
					double xDist = c1.getX() + c1.getWidth() / 2
							- (c2.getX() + c2.getWidth() / 2);
					double yDist = c1.getY() + c1.getHeight() / 2
							- (c2.getY() + c2.getHeight() / 2);
					double dist = (float) Math.sqrt(xDist * xDist + yDist
							* yDist)
							- c1.getSize() - c2.getSize();

					if (dist < 0) {
						float moveX = c1.getX(), moveY = c1.getY();
						float mx = (float) (xDist - c1.getWidth()), my = (float) (yDist - c1
								.getHeight());
						// x1 < x2
						if (xDist < 0) {
							moveX += (float) (c1.getForce().dx > 0 ? -mx : mx);
							// x1 > x2
						} else {
							moveX += (float) (c1.getForce().dx > 0 ? mx : -mx);
						}

						// y1 < y2
						if (yDist < 0) {
							moveY += (float) (c1.getForce().dy > 0 ? -my : my);
						} else {
							moveY += (float) (c1.getForce().dy > 0 ? my : -my);
						}
						if (c1.getId().equals("c1"))
						if (c2.getId().equals("c2"))

						if (moveX > xMargin && moveX < xMargin + width) {
							c1.moveTo(moveX, c1.getY());
						}

						if (moveY > 0) {
							c1.moveTo(c1.getX(), moveY);
						}
					}
				}
			}

			// Repulsion
			if (!noRepulsion) {
				for (int m = 0; m < connectables.length; m++) {
					ConnectableObject b1 = connectables[m];
					b1.t = maxDisplace
							* Math.pow(i / (double) iterations, coolExp);
					for (int n = m + 1; n < connectables.length; n++) {
						ConnectableObject b2 = connectables[n];
						// float repulsiveF = k * k * (1f / dist - dist * dist /
						// repulseRadio);
						//repulsion(b1, b2, rep * (1 + diagram.getDegreeOf(b1)) * (1 + diagram.getDegreeOf(b1)), overlap);
						repulsion(b1, b2, k, overlap);
					}
				}

				// gravity
				for (ConnectableObject c : connectables) {
					Force cForce = c.getForce();
					float xCenter = width / 2;
					float yCenter = height / 2;
					float d = (float) Math.sqrt(Math.pow(c.getX() - xCenter
							- xMargin, 2)
							+ Math.pow(c.getY() - yCenter, 2));
					// center AREA not point
					if (d > gravityRadius) {
						/*float gf = diagram.getDegreeOf(c) == 0 ? k * (float) gravity * d : 
							0.01f * k * (float) gravity * d;*/
						float gf = 0.02f * k * (float) gravity * d / (diagram.getDegreeOf(c) + 1);
						float gX = c.getX() < xCenter + xMargin ? gf * c.getX()
								: -gf * c.getX();
						float gY = c.getY() < yCenter ? gf * c.getY() : -gf
								* c.getY();
						cForce.dx += gX;
						cForce.dy += gY;
					}
				}
			}

			for (ConnectableObject c : connectables) {
				Force cForce = c.getForce();
				float xDist = cForce.dx;
				float yDist = cForce.dy;
				float dist = (float) Math.sqrt(cForce.dx * cForce.dx
						+ cForce.dy * cForce.dy);
				// COOL
				/*
				 * if(dist>c.t){ dist=(float) (c.t/dist); xDist*=dist;
				 * yDist*=dist; }
				 */
				// MOVE
				if (dist > 0 && !c.isFixed()) {
					float limitedDist = Math.min(maxDisplace, dist);
					float newX = c.getX() + xDist / dist * limitedDist;
					float newY = c.getY() + yDist / dist * limitedDist;
					if (newX > xMargin) {
						if (!(xMargin > 0 && newX > xMargin + width - c.getWidth())) c.moveTo(newX, c.getY());
					}
					if (newY > 0) c.moveTo(c.getX(), newY);
					
				}
			}
		}
	}

	private static void attraction_noCollition(ConnectableObject c1,
			ConnectableObject c2, double c, boolean overlap) {
		double xDist = c1.getX() - c2.getX();
		double yDist = c1.getY() - c2.getY();
		double dist = (float) Math.sqrt(xDist * xDist + yDist * yDist)
				- c1.getSize() - c2.getSize();

		if (dist <= 0 && !overlap)
			return;
		//double f = 10 * dist * dist / k;
		double f = 0.01 * -c * dist;
		Force c1Force = c1.getForce();
		Force c2Force = c2.getForce();
		c1Force.dx += xDist / dist * f;
		c1Force.dy += yDist / dist * f;
		c2Force.dx -= xDist / dist * f;
		c2Force.dy -= yDist / dist * f;
		
	}

	/*private static void repulsion(ConnectableObject c1, ConnectableObject c2,
			double rep, double k, boolean overlap) {
		double xDist = c1.getX() + c1.getWidth() / 2
				- (c2.getX() + c2.getWidth() / 2);
		double yDist = c1.getY() + c1.getHeight() / 2
				- (c2.getY() + c2.getHeight() / 2);
		double dist = (float) Math.sqrt(xDist * xDist + yDist * yDist)
				- c1.getSize() - c2.getSize();

		double f;
		//double repForce = k * k / dist;
		double repForce = -10000 / Math.pow(dist, 2);
		Force c1Force = c1.getForce();
		Force c2Force = c2.getForce();

		if (dist > 0 || overlap) {
			repForce = k * k / dist;
			f = repForce;
		} else {
			repForce = k * k * dist * 1000;
			f = repForce;
		}
		
		if (dist != 0) {
			c1Force.dx += xDist / dist * f;
			c1Force.dy += yDist / dist * f;

			c2Force.dx -= xDist / dist * f;
			c2Force.dy -= yDist / dist * f;
		}

	}*/
	private static void repulsion(ConnectableObject c1, ConnectableObject c2,
			double k, boolean overlap) {
		double xDist = c1.getX() + c1.getWidth() / 2
				- (c2.getX() + c2.getWidth() / 2);
		double yDist = c1.getY() + c1.getHeight() / 2
				- (c2.getY() + c2.getHeight() / 2);
		double dist = (float) Math.sqrt(xDist * xDist + yDist * yDist)
				- c1.getSize() - c2.getSize();

		double f;
		//double repForce = k * k / dist;
		double repForce = -10000 / Math.pow(dist, 2);
		Force c1Force = c1.getForce();
		Force c2Force = c2.getForce();

		if (dist > 0 || overlap) {
			repForce = k * k / dist;
			f = repForce;
		} else {
			repForce = k * k * dist * 1000;
			f = repForce;
		}
		
		if (dist != 0) {
			c1Force.dx += xDist / dist * f;
			c1Force.dy += yDist / dist * f;

			c2Force.dx -= xDist / dist * f;
			c2Force.dy -= yDist / dist * f;
		}

	}
	
	
	public static void moveToClosestCell(List<ConnectableObject> conns, int cellSize) {
		for (int i = 0; i < conns.size(); i++) {
			ConnectableObject c = conns.get(i);
			float cellOffX = c.getX() % cellSize;
			float cellOffY = c.getY() % cellSize;
			if (cellOffX > 0) { //No está bien posicionado en X
				int posX = (int) (cellOffX < (float)cellSize / 2? c.getX() - cellOffX: c.getX() + cellSize - cellOffX);
				//System.out.println("x: " + c.getX() + " off: " + cellOffX + " total: " + (posX));
				c.moveTo(posX, c.getY());
			}
			if (cellOffY > 0) {
				int posY = (int) (cellOffY < (float)cellSize / 2? c.getY() - cellOffY: c.getY() + cellSize - cellOffY);
				c.moveTo(c.getX(), posY);
			}
			for (int j = i+1; j < conns.size(); j++) {
				ConnectableObject cc = conns.get(j);
				while (c.collides(cc)){
					int posX = c.getX() < cc.getX()? (int) (c.getX() - cellSize):  (int) (c.getX() + cellSize);
					int posY = c.getY() < cc.getY()? (int) (c.getY() - cellSize):  (int) (c.getY() + cellSize);
					c.moveTo(posX, posY);
				}
			}
		}
	}

	public static int score(Diagram d, int scoreLine, int scoreRect){
		int score = 0;
		List<Connection> connections = d.getConnections();
		for (int i = 0; i < connections.size(); i++) {
			Connection c1 = connections.get(i);
			Line2D l1 = new Line2D.Double(c1.getStart(), c1.getGoal());
			for (int j = i + 1; j < connections.size(); j++) {
				Connection c2 = connections.get(j);
				Line2D l2 = new Line2D.Double(c2.getStart(), c2.getGoal());;
				if (l1.intersectsLine(l2) 
						&& !l1.getP1().equals(l2.getP1()) 
						&& !l1.getP2().equals(l2.getP2())
						&& !l1.getP1().equals(l2.getP2())
						&& !l1.getP2().equals(l2.getP1())) {
					score += scoreLine;
					//System.out.println(c1.getFrom() + " - " + c1.getTo() + " CON " + c2.getFrom() + " - " + c2.getTo());
				}
			}
			for (ConnectableObject c : d.getConnectables()) {
				if (l1.intersects(c.getX()+1, c.getY()+1, c.getWidth()-2, c.getHeight()-2)) {
					//System.out.println(c.getId());
					score += scoreRect;
				}
			}
		}
		
		return score;
	}
	
	public static int score(ArrayList<Path> paths, int scoreForIntersection, int scoreForLength){
		int score = 0;
		for (int i = 0; i < paths.size(); i++) {
			Path p1 = paths.get(i);
			score += p1.getElements().size();
			for (int j = i + 1; j < paths.size(); j++) {
				Path p2 = paths.get(j);
				if (p1.intersects(p2.getBoundsInParent())) {
					score += scoreForIntersection;
				}
			}
		}
		return score;
	}
}
