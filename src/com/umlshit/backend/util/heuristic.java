package com.umlshit.backend.util;

import java.awt.Point;

public class heuristic implements AStarHeuristic {
	public float getEstimatedDistanceToGoal(Point start, Point goal) {
		int dx = Math.abs(start.x - goal.x);
		int dy = Math.abs(start.y - goal.y);
		return (dx + dy);
	}
}
