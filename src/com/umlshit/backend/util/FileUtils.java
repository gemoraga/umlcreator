package com.umlshit.backend.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import javax.imageio.ImageIO;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class FileUtils {

	
    public static File OpenFile(Stage s, String ext1, String ext2) {
    	FileChooser fc = new FileChooser();
    	//Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(ext1, ext2);
        fc.getExtensionFilters().add(extFilter);
        
    	File file = fc.showOpenDialog(s);
        return file;
    }
    
    public static void SaveFile(Stage s, String content, String ext1, String ext2){
    	FileChooser fc = new FileChooser();
    	//Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(ext1, ext2);
        fc.getExtensionFilters().add(extFilter);
        
        //Show save file dialog
        File file = fc.showSaveDialog(s);
        
        if (file == null) return;
        try {
            OutputStreamWriter fileWriter = new OutputStreamWriter(
            	     new FileOutputStream(file),
            	     Charset.forName("UTF-8").newEncoder() 
            	 );
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
        }
         
    }
    
    public static void SavePNG(Stage s, WritableImage image, String ext1, String ext2){
    	FileChooser fc = new FileChooser();
    	//Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(ext1, ext2);
        fc.getExtensionFilters().add(extFilter);
        
        //Show save file dialog
        File file = fc.showSaveDialog(s);
        if (file == null) return;
        try {
        	ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException ex) {
        }
         
    }
}
