package com.umlshit.backend.Class;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.umlshit.backend.Box;
import com.umlshit.backend.ConnectableObject;


public class ClassBox extends Box {
	private List<Attribute> attributes;
	private List<Method> methods;
	private List<Point> usedAnchors;
	
	public ClassBox(String id, String name){
		super(id, name);
		
		attributes = new ArrayList<Attribute>();
		methods = new ArrayList<Method>();
		usedAnchors = new ArrayList<Point>();
	}
	
	public ClassBox(ClassBox b){
		super(b.id, b.name, b.x, b.y, b.getWidth(), b.getHeight());
		attributes = b.attributes;
		methods = b.methods;
		usedAnchors = new ArrayList<Point>();
	}


	public String getName(){
		return name;
	}
	public List<Method> getMethods(){
		return methods;
	}
	public List<Attribute> getAttributes(){
		return attributes;
	}
	public void addAttribute(Attribute a){
		attributes.add(a);
	}
	
	@Override public String toString(){
		StringBuilder result = new StringBuilder();
	    String NEW_LINE = System.getProperty("line.separator");
	    
	    result.append("  Class Name: " + getName() + NEW_LINE);
	    result.append("  Attributes:"+NEW_LINE);
		for (Attribute a : attributes) {
			result.append("    " + a.toString() + NEW_LINE);
		}
		result.append("  Methods:"+NEW_LINE);
		for (Method m : methods) {
			result.append("    " + m.toString() +  NEW_LINE);
		}
		return result.toString();

	}
	
	public void addMethod(Method m){
		methods.add(m);
	}

	@Override public ConnectableObject Clone(){
		return new ClassBox(this);
	}
	
	public List<Point> getPossibleAnchors(int cellSize){
		List<Point> points = new ArrayList<Point>();
		
		//para x fijo (0 y width)
		for (int j = (int) (getY() / cellSize); j < (getY() + getHeight())/cellSize; j++) {
			points.add(new Point((int) (getX() / cellSize), j));
			points.add(new Point((int) ((getX() + getWidth())/cellSize-1), j));
		}
		
		//para y fijo (0 y height)
		for (int i = (int) (getX() / cellSize); i < (getX() + getWidth())/cellSize; i++) {
			points.add(new Point(i, (int) (getY() / cellSize)));
			points.add(new Point(i, (int) ((getY() + getHeight())/cellSize - 1)));
		}
		
		return points;
	}
	
	public static Point[] bestAnchors(ClassBox c1, ClassBox c2, int cellSize) {
		Point[] points = new Point[2];
		int manhattan = Integer.MAX_VALUE;
		List<Point> anchors1 = c1.getPossibleAnchors(cellSize);
		List<Point> anchors2 = c2.getPossibleAnchors(cellSize);
		int i = 0, j = 0;
		while (i < anchors1.size() * 2) {
			Point p1 = anchors1.get(i%anchors1.size());
			if ((c1.usedAnchors.contains(p1) || usedAnchorNear(5, p1, c1.usedAnchors)) && ! (i >= anchors1.size())) { i++; continue;}
			while (j < anchors2.size() * 2) {
				Point p2 = anchors2.get(j%anchors2.size());
				if ((c2.usedAnchors.contains(p2) || usedAnchorNear(5, p2, c2.usedAnchors))&& ! (j >= anchors2.size())) { j++; continue;}
				int current = Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
				if (current < manhattan) {
					manhattan = current;
					points[0] = new Point(p1.x, p1.y); 
					points[1] = new Point(p2.x, p2.y);
				}
				j++;
			}
			j = 0;
			i++;
		}
		c1.usedAnchors.add(points[0]);
		c2.usedAnchors.add(points[1]);
		return new Point[] {new Point(points[0].x*cellSize, points[0].y*cellSize), new Point(points[1].x*cellSize, points[1].y*cellSize)};
		
	}
	
	private static boolean usedAnchorNear(int dist, Point p, List<Point> usedAnchors) {
		for (Point used : usedAnchors) {
			float manhattan = Math.abs(used.x - p.x) + Math.abs(used.y - p.y);
			if (manhattan <= dist) return true;
		}
		return false;
	}
}
