package com.umlshit.backend.Class;

import com.umlshit.backend.Connection;
import com.umlshit.backend.Diagram;
import com.umlshit.backend.Box;

public class ClassDiagram extends Diagram{
	public ClassDiagram(String name) {
		super(name);
	}

	public static enum visibility{
		Public,
		Private,
		Protected
	}

	@Override public String toString() {
		StringBuilder result = new StringBuilder();
	    String NEW_LINE = System.getProperty("line.separator");
	    
	    result.append("Diagram Name: " + this.name + NEW_LINE);
	    
	    result.append(" Classes:" + NEW_LINE);

	    for (int i = 0; i < boxes.size(); i++) {
	    	ClassBox cBox = (ClassBox) boxes.get(i);
	    	result.append(cBox.toString() + NEW_LINE);
	    } 	

	    
		return result.toString();
	}

	@Override
	public Diagram Clone() {
		ClassDiagram clone = new ClassDiagram(this.name);
		clone.resize(width, height);
		for (Box b : this.boxes) {
			clone.boxes.add((Box) b.Clone());
		}
		for (Connection c: this.connections) {
			clone.connections.add(c.Clone());
		}
		return clone;
	}

}



