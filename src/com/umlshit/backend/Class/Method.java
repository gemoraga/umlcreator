package com.umlshit.backend.Class;

import java.util.ArrayList;
import java.util.List;

public class Method {
	private String name;
	private List<Parameter> parameters;
	private String type;
	private ClassDiagram.visibility visibility;
	

	public Method(String name, String type, ClassDiagram.visibility visibility){
		this.name = name;
		this.type = type;
		this.visibility = visibility;
		parameters = new ArrayList<Parameter>();
	}
	
	public void addParameter(Parameter p){
		parameters.add(p);
	}
	
	public String getName() {
		return name;
	}
	
	public List<Parameter> getParameters() {
		return parameters;
	}
	
	public String getType(){
		return type;
	}
	
	@Override public String toString(){
		StringBuilder result = new StringBuilder();
		if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Public){
			result.append("+ ");
		}
		else if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Private){
			result.append("- ");
		}
		else if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Protected){
			result.append("# ");
		}
		result.append(getName() + "(");
		List<Parameter> params = getParameters();
		for (int i = 0 ; i < params.size(); i++) {
			result.append(params.get(i).getName() + ":" + params.get(i).getType());
			if (i < params.size()-1) result.append(", ");
		}
		result.append("):" + getType());
		
		return result.toString();
	}
}
