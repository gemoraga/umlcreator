package com.umlshit.backend.Class;

public class Attribute {
	private String name;
	private String type;
	private ClassDiagram.visibility visibility;
	
public Attribute(String name, String type, ClassDiagram.visibility visibility){
	this.name = name;
	this.type = type;
	this.visibility = visibility;
}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	@Override public String toString(){
		StringBuilder result = new StringBuilder();
		if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Public){
			result.append("+ ");
		}
		else if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Private){
			result.append("- ");
		}
		else if(visibility==com.umlshit.backend.Class.ClassDiagram.visibility.Protected){
			result.append("# ");
		}
		result.append(getName() + ":" + getType());
		return result.toString();
	}

}
