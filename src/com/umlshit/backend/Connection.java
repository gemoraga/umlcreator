package com.umlshit.backend;

import java.awt.geom.Point2D;

public class Connection {
	private String from;
	private String to;
	private Type type;
	private Point2D start;
	private Point2D goal;
	
	public enum Type {
		isa, basic, extend, include, dependency, association, aggregation, composition, inheritance
	}
	public void setType(Type t){
		type=t;
		
	}
	
	public Connection(String idFrom, String idTo){
		this.from = idFrom;
		this.to = idTo;
	}
	
	public String getFrom(){
		return from;
	}
	
	public String getTo() {
		return to;
	}
	public Type getType(){
		return type;
	}
	
	

	public Point2D getStart() {
		return start;
	}

	public void setStart(Point2D start) {
		this.start = start;
	}

	public Point2D getGoal() {
		return goal;
	}

	public void setGoal(Point2D goal) {
		this.goal = goal;
	}

	public Connection Clone() {
		Connection c = new Connection(from, to);
		c.setType(type);
		c.setStart(start);
		c.setGoal(goal);
		return c;
	}
	
}
