package com.umlshit.backend;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Diagram {
	protected String name;
	protected List<Box> boxes;
	protected List<Connection> connections;
	protected float width;
	protected float height;
	
	
	public Diagram(String name) {
		this.name = name;
		boxes = new ArrayList<Box>();
		connections = new ArrayList<Connection>();
	}
	
	public String getName(){
		return name;
	}
	
	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public void resize(float width, float height) {
		this.width = width;
		this.height = height;
	}
	
	//Boxes
	public void addBox(Box box) {
		boxes.add(box);
	}
	
	public Box getBoxById(String id){
		Iterator<Box> it = boxes.iterator();
		while(it.hasNext()) {
			Box box = it.next();
			if (box.getId().equals(id)) return box;
		}
		return null;
	}
	
	public ConnectableObject getConnById(String id){
		Iterator<ConnectableObject> it = this.getConnectables().iterator();
		while(it.hasNext()) {
			ConnectableObject c = it.next();
			if (c.getId().equals(id)) return c;
		}
		return null;
	}
	
	public int getDegreeOf(ConnectableObject c) {
		int degree = 0;
		for (Connection con : connections) {
			if (con.getFrom().equals(c.getId()) || con.getTo().equals(c.getId())) degree++;
		}
		return degree;
	}
	
	//Connections
	public void addConnection(Connection connection) {
		connections.add(connection);
	}
	
	public List<Box> getBoxes(){
		return boxes;
	}
	
	public List<Connection> getConnections(){
		return connections;
	}
	
	public List<ConnectableObject> getConnectables() {
		List<ConnectableObject> connectables = new ArrayList<ConnectableObject>();
		connectables.addAll(boxes);
		return connectables;
	}
	
	//Clone
	public abstract Diagram Clone();
}
