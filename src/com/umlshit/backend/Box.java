package com.umlshit.backend;

public abstract class Box extends ConnectableObject{
	protected String name;

	public Box(String id, String name) {
		super(id);
		this.name = name;

	}
	
	protected Box(String id, String name, float x, float y, double width, double height) {
		super(id, x, y, width, height);
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
