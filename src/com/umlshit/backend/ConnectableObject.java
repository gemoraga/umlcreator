package com.umlshit.backend;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public abstract class ConnectableObject{
	protected String id;
	protected float x;
	protected float y;
	protected double width;
	protected double height;
	public double t; //temperature
	protected boolean fixedPosition;
	protected Force force;
	
	protected ConnectableObject(String id){
		this.id = id;
		force = new Force();
		t = 0;
	}
	
	protected ConnectableObject(String id, float x, float y, double width, double height){
		this.id = id;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		force = new Force();
		t = 0;
	}
	
	public String getId(){
		return this.id;
	}
	
	public boolean isFixed() {
		return fixedPosition;
	}
	
	public void moveTo(float x, float y){
		this.x = x; 
		this.y = y;
	}
	
	public void resize(double d, double e) {
		this.width = d;
		this.height = e;
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	//Aproximating as circle
	public float getSize() {
		return (float) Math.sqrt((width * width + height * height) /4);
	}
	public Force getForce() {
		return force;
	}


	public boolean collides(ConnectableObject c) {
		Rectangle rect1 = new Rectangle((int) this.x,(int) this.y, (int)this.width,(int) this.height);
		Rectangle rect2 = new Rectangle((int) c.x,(int) c.y, (int)c.width,(int) c.height);
		return rect1.intersects(rect2);
	}

	
	public abstract ConnectableObject Clone();


	public ArrayList<Line2D> getBounds(){
		ArrayList<Line2D> bounds = new ArrayList<Line2D>();
		bounds.add(new Line2D.Double(x,y,x+width, y));
		bounds.add(new Line2D.Double(x,y+height,x+width, y+height));
		bounds.add(new Line2D.Double(x,y,x, y+height));
		bounds.add(new Line2D.Double(x+width,y,x+width, y+height));
		return bounds;
	}
}

